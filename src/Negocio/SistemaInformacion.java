/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Materia;

/**
 * Tenemos 3 materias
 *
 * @author madarme
 */
public class SistemaInformacion {

    private Materia myMaterias[] = new Materia[3];
    private String urlProgramacionIII;
    private String urlMatematicaIII;
    private String urlFisicaII;

    public SistemaInformacion() {
    }

    /**
     * Solución del punto 1
     *
     * @param urlProgramacionIII cadena con la url con la información de
     * estudiantes de programación III
     * @param urlMatematicaIII cadena con la url con la información de
     * estudiantes de matemáticas III
     * @param urlFisicaII cadena con la url con la información de estudiantes de
     * física II
     */
    public SistemaInformacion(String urlProgramacionIII, String urlMatematicaIII, String urlFisicaII) {
        this.urlProgramacionIII = urlProgramacionIII;
        this.urlMatematicaIII = urlMatematicaIII;
        this.urlFisicaII = urlFisicaII;

        this.crearMateria(1, "Matemáticas III", this.urlMatematicaIII);
        this.crearMateria(2, "Fisica II", this.urlFisicaII);
        this.crearMateria(3, "Progam III", this.urlProgramacionIII);
    }

    /**
     * Crea un materia
     *
     * @param codigo un entero con el código de la materia
     * @param nombreMateria una cadena que contiene el nombre de la materia
     * @param url una cadena que contiene la URL de un archivo csv con la
     * información de los estudiantes de esa materia
     */
    private void crearMateria(int codigo, String nombreMateria, String url) {

        Materia materiaCreada = new Materia(codigo, nombreMateria, url);
        for (int i = 0; i < myMaterias.length; i++) {
            if (myMaterias[i] == null) {
                myMaterias[i] = materiaCreada;
                break;
            }
        }
    }

//    private int tamañoMayor(Estudiante[] lista1, Estudiante[] lista2) {
//        int tamañoMayor = 0;
//        if (lista1.length >= lista2.length) {
//            tamañoMayor = lista1.length;
//        } else {
//            tamañoMayor = lista2.length;
//        }
//        return tamañoMayor;
//    }

//    private Estudiante[] getEstudiantesEnEnoPeroNoEnOtro(Estudiante[] lista1, Estudiante[] lista2) {
//
//        Estudiante[] resultado = new Estudiante[tamañoMayor(lista1, lista2)];
//        int contador = 0;
//        for (Estudiante estudiante : lista1) {
//            boolean esEncontrado = false;
//            for (Estudiante estudiante2 : lista2) {
//                if (estudiante2.equals(estudiante)) {
//                    esEncontrado = true;
//                    break;
//                }
//            }
//            if (!esEncontrado) {
//                resultado[contador++] = estudiante;
//            }
//        }
//
//        return resultado;
//
//    }
    private boolean estaEstudiante(Estudiante[] lista1, Estudiante x) {

        boolean esEncontrado = false;
        for (Estudiante estudiante : lista1) {
            if (estudiante.equals(x)) {
                esEncontrado = true;
            }
        }
        return esEncontrado;
    }

    /**
     * @return un listado de estudiantes
     */
    public Estudiante[] getListadoProgIII_FisicaII_NO_MatIII() {
        Estudiante[] matematicas = myMaterias[0].getMyEstudiantes();
        Estudiante[] fisica = myMaterias[1].getMyEstudiantes();
        Estudiante[] programacion = myMaterias[2].getMyEstudiantes();
        

        Estudiante[] no_matIII = new Estudiante[programacion.length];
        int contador = 0;
        for (Estudiante est : programacion) {
            if(!estaEstudiante(matematicas, est) && estaEstudiante(fisica, est))
            no_matIII[contador++]= est;
        }
         Estudiante[] resultado = new Estudiante[contador];
         for (int i = 0; i < contador; i++) {
            resultado[i] = no_matIII[i];
        }
        return resultado;
    }

    /**
     * Punto 3.	Crear un vector con los estudiantes que tienen matriculado
     * programación III y Matemáticas III pero no física II.
     *
     * @return
     */
    public Estudiante[] getListadoProgIII_MatIII_NO_fisII() {
//        mtematicas ->0
//        fisica ->1
//        programacion ->2
        Estudiante[] matematicas = myMaterias[0].getMyEstudiantes();
        Estudiante[] fisica = myMaterias[1].getMyEstudiantes();
        Estudiante[] programacion = myMaterias[2].getMyEstudiantes();
        

        Estudiante[] no_fisII = new Estudiante[programacion.length];
        int contador = 0;
        for (Estudiante est : programacion) {
            if(!estaEstudiante(fisica, est) && estaEstudiante(matematicas, est))
            no_fisII[contador++]= est;
        }
         Estudiante[] resultado = new Estudiante[contador];
         for (int i = 0; i < contador; i++) {
            resultado[i] = no_fisII[i];
        }
        return resultado;

    }

    /**
     * Punto 4.	Crear un vector con los estudiantes que están viendo dos de las
     * tres materias (Cualquiera que sea).
     *
     * @return
     */
    public Estudiante[] getListadoVer_UNICAMENTE_dosMaterias() {

        Estudiante[] resultado = new Estudiante[getListadoProgIII_FisicaII_NO_MatIII().length + getListadoProgIII_MatIII_NO_fisII().length];
        Estudiante[] noFis = getListadoProgIII_MatIII_NO_fisII();
        Estudiante[] noMat = getListadoProgIII_FisicaII_NO_MatIII();

        //resultado = getListadoProgIII_FisicaII_NO_MatIII();
        for (int i = 0; i < noMat.length; i++) {
            resultado[i] = noMat[i];

        }
        for (int i = noMat.length, j = 0; i < resultado.length; i++, j++) {
            resultado[i] = noFis[j];
        }
        return resultado;
    }

    /**
     * Obtiene la información de todos los estudiantes por materia
     *
     * @return una cadena con los 3 listados de las materias
     */
    @Override
    public String toString() {
        String msg = "";
        for (Materia f : this.myMaterias) {
            msg += f.toString() + ";";
        }
        return msg;

    }

}

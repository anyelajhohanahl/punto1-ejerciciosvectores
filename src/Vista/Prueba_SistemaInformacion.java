/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.SistemaInformacion;

/**
 *
 * @author madar
 */
public class Prueba_SistemaInformacion {

    public static void main(String[] args) {
        String url1 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/listadoProgramacionIII.csv";
        String url2 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/matematicaIII.csv";
        String url3 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/fisicaII.csv";

        SistemaInformacion mySistema = new SistemaInformacion(url1, url2, url3);
        /**
         * :) Debe llamar los métodos e imprimir el resultado de los vectores
         * resultantes
         *
         */
        mySistema.getListadoProgIII_FisicaII_NO_MatIII();
        System.out.println("getListadoProgIII_FisicaII_NO_MatIII()");
        for (Estudiante est : mySistema.getListadoProgIII_FisicaII_NO_MatIII()) {
            System.out.println(est.toString());
        }
        mySistema.getListadoProgIII_MatIII_NO_fisII();
        System.out.println("etListadoProgIII_MatIII_NO_fisII()");
        for (Estudiante estudi : mySistema.getListadoProgIII_MatIII_NO_fisII()) {
            System.out.println(estudi.toString());
        }
        mySistema.getListadoVer_UNICAMENTE_dosMaterias();
        System.out.println("getListadoVer_UNICAMENTE_dosMaterias()");
        for (Estudiante estudiant : mySistema.getListadoVer_UNICAMENTE_dosMaterias()) {
            System.out.println(estudiant.toString());
        }

    }
}

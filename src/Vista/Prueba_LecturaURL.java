/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Prueba_LecturaURL {
    
    public static void main(String[] args) {
        
        String url1="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/listadoProgramacionIII.csv";
        String url2="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/matematicaIII.csv";
        String url3="https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/fisicaII.csv";
        
        ArchivoLeerURL archivo=new ArchivoLeerURL(url1);
        Object datos[]=archivo.leerArchivo();
        imprimir(datos,url1);
        
        archivo=new ArchivoLeerURL(url2);
        datos=archivo.leerArchivo();
        imprimir(datos,url2);
        
        archivo=new ArchivoLeerURL(url3);
        datos=archivo.leerArchivo();
        imprimir(datos,url3);
    
        
    }
    
    private static void imprimir(Object []datos, String url)
    {
        System.out.println("\n\nLeyendo archivo de URL:"+url);
        int i=0;
          for(Object x:datos)
            System.out.println("Fila["+(i++)+"]:"+x.toString());
    }
}

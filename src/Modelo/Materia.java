/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Arrays;
import java.util.Objects;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Materia {

    private int codMateria;
    private String nombreMateria;
    private Estudiante myEstudiantes[];

    public Materia() {
    }

    public Materia(int codMateria, String nombreMateria, Estudiante[] myEstudiantes) {
        this.codMateria = codMateria;
        this.nombreMateria = nombreMateria;
        this.myEstudiantes = myEstudiantes;
    }
    
    public Materia(int codMateria, String nombreMateria,String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object datos[] = archivo.leerArchivo();
        if(datos.length ==0){
            throw new RuntimeException("url sin datos");
        }
        this.myEstudiantes = new Estudiante[datos.length - 1];
        for (int i = 1; i <= this.myEstudiantes.length; i++) {
            String filaDatos = datos[i].toString();
            this.myEstudiantes[i-1] = new Estudiante(filaDatos);
//            System.out.println(this.myEstudiantes[i-1] = new Estudiante(filaDatos));
        }
        this.codMateria = codMateria;
        this.nombreMateria = nombreMateria;
    }


    /**
     * @return the codMateria
     */
    public int getCodMateria() {
        return codMateria;
    }

    /**
     * @param codMateria the codMateria to set
     */
    public void setCodMateria(int codMateria) {
        this.codMateria = codMateria;
    }

    /**
     * @return the nombreMateria
     */
    public String getNombreMateria() {
        return nombreMateria;
    }

    /**
     * @param nombreMateria the nombreMateria to set
     */
    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }
    
    private void validar(int i) {
        if (this.myEstudiantes == null || i < 0 || i >= this.myEstudiantes.length) {
            throw new RuntimeException("Índice fuera de rango");
        }
    }
    
//    public int length()
//    {
//        return this.myEstudiantes.length;
//    }

    /**
     
     * @return the myEstudiantes
     */
    public Estudiante[] getMyEstudiantes() {
        return myEstudiantes;
    }

    /**
     * @param i posicion en el arreglo
     * @param codigoEstudiante
     * @param nombreEstudiante
     * @param emailEstudiante
     */
    public void setMyEstudiantes(int i, int codigoEstudiante, String nombreEstudiante, String emailEstudiante) {
        validar(i);
        this.myEstudiantes[i] = new Estudiante(codigoEstudiante, nombreEstudiante, emailEstudiante);
    }
    
    public int getTamFilas() {
        if (this.myEstudiantes == null) {
            throw new RuntimeException("Error :( vacía");
        }
        return this.myEstudiantes.length;
    }
//
//    public int getTamColumnas() {
//        if (this.myEstudiantes == null) {
//            throw new RuntimeException("Error matriz vacía");
//        }
//        return this.myEstudiantes[0].l;
//    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Materia other = (Materia) obj;
        if (this.codMateria != other.codMateria) {
            return false;
        }
        if (!this.nombreMateria.equals(other.nombreMateria)) {
            return false;
        }
        for (int i = 0; i < myEstudiantes.length; i++) {
            if (!this.myEstudiantes[i].equals(other.myEstudiantes[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Materia :" + codMateria + ";" + nombreMateria + ";" + myEstudiantes + ';';
    }

}
    
    
    


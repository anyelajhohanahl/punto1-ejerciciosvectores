/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *
 * @author madar
 */
public class Estudiante {

    private int codigo;
    private String nombre;
    private String email;

    public Estudiante() {
    }

    public Estudiante(String datos) {
        String dt[] = datos.split(";");
        this.codigo = Integer.parseInt(dt[0]);
        this.nombre = dt[1];
        this.email = dt[2];
    }

    public Estudiante(int codigo, String nombre, String email) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.email = email;
    }
        
    public void validarCodigo(int i){
        if(this.codigo ==0){
            throw new RuntimeException("codigo no valido");
        }
    }
    
    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.validarCodigo(codigo);
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.codigo + ";" + this.nombre + ";" + this.email + ";";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        if (!this.nombre.equals(other.nombre)) {
            return false;
        }
        if (!this.email.equals(other.email)) {
            return false;
        }
        return true;
    }

    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            throw new RuntimeException("No se pueden comparar");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("No se pueden comparar");
        }
        final Estudiante other = (Estudiante) obj;
        int resta = this.codigo - other.codigo;
        
        if (resta == 0) {
            return 0;
        }
        if (resta > 0) {
            return 1;
        }
        return -1;
    }

}
